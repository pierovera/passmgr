#include <math.h> 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <time.h> 
#include <unistd.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/kdf.h>
#include <openssl/rand.h>

#define PM_HASH_SIZE 32
#define PM_SALT_SIZE 16
#define PM_MKEY_SIZE 64
#define PM_KEY_SIZE 32
#define PM_IV_SIZE 16
#define PM_TAG_SIZE 16
#define PM_PASS_HASH_RDS 4096
#define PM_ROUNDS 2048

typedef struct UserPass {
    char *user;
    char *pass;
    size_t usersz;
    size_t passsz;
} userPass;

char *getPass(int confirm) {
    static struct termios oldc, newc;
    char c;
    size_t sz = 256, pos = 0;
    char *pass = calloc(sz, sizeof(char));

    printf("Password: ");

    tcgetattr(STDIN_FILENO, &oldc);
    newc = oldc;
    newc.c_lflag &= ~(ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newc);

    while ((c = getchar()) != '\n' && c != EOF) {
        pass[pos] = c;
        
        if (++pos == sz) {
            sz *= 2;
            pass = reallocarray(pass, sz, sizeof(char));
        }
    }
    pass[pos] = '\0';

    tcsetattr(STDIN_FILENO, TCSANOW, &oldc);
    printf("\n");

    if (confirm) {
        char *passConf = calloc(sz, sizeof(char));
        pos = 0;
        
        printf("Confirm password: ");
        tcgetattr(STDIN_FILENO, &oldc);
        newc = oldc;
        newc.c_lflag &= ~(ECHO);
        tcsetattr(STDIN_FILENO, TCSANOW, &newc);

        while ((c = getchar()) != '\n' && c != EOF) {
            passConf[pos] = c;
        
            if (++pos == sz) {
                sz *= 2;
                passConf = reallocarray(passConf, sz, sizeof(char));
            }
        }
        passConf[pos] = '\0';
        tcsetattr(STDIN_FILENO, TCSANOW, &oldc);
        printf("\n");

        for (int i = 0; ; i++) {
            if ((pass[i] == '\0') && (passConf[i] == '\0'))
                break;
            if (pass[i] != passConf[i]) {
                printf("Passwords do not match.\n");
                free(pass);
                free(passConf);
                return NULL;
            }
        }

        free(passConf);
    }   

    return pass;
}

unsigned char *hexToBytes(const char *hex, size_t len) {
    unsigned char *bytes = calloc(len/2, sizeof(char));
    const char *p = hex;
    
    for (size_t i = 0; i < len/2; i++, p += 2)
        sscanf(p, "%2hhx", &bytes[i]);

    return bytes;
}

unsigned char *passToKey(const char *pass, const unsigned char* salt, int passlen) {
    size_t keylen = PM_MKEY_SIZE;
    unsigned char *out = malloc(keylen);
    
    PKCS5_PBKDF2_HMAC(pass, passlen, salt, PM_SALT_SIZE, PM_ROUNDS,
                      EVP_sha256(), keylen, out);
     
    return out;
}

unsigned char *passToHash(const char *pass, const unsigned char *salt, size_t passlen,
                          size_t saltlen) {
    unsigned int mdlen;
    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    const EVP_MD *md = EVP_sha256();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, pass, passlen);
    EVP_DigestUpdate(mdctx, salt, saltlen);

    unsigned char *digest = (unsigned char *)OPENSSL_malloc(EVP_MD_size(md));
    EVP_DigestFinal_ex(mdctx, digest, &mdlen);

    EVP_MD_CTX_free(mdctx);
    
    return digest;
}

EVP_CIPHER_CTX *getEncryptCTX(const unsigned char *mkey, const unsigned char *salt) {
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    size_t keylen = PM_KEY_SIZE + PM_IV_SIZE;
    unsigned char *key = malloc(keylen);

    PKCS5_PBKDF2_HMAC((const char *)mkey, PM_MKEY_SIZE, salt, PM_SALT_SIZE, PM_ROUNDS,
                      EVP_sha256(), keylen, key);
    
    EVP_CIPHER_CTX_init(ctx);
    EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, PM_IV_SIZE, NULL);
    EVP_EncryptInit_ex(ctx, NULL, NULL, key, key + PM_KEY_SIZE);

    free(key);
    
    return ctx;
}

EVP_CIPHER_CTX *getDecryptCTX(const unsigned char *mkey, const unsigned char *salt) {
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    size_t keylen = PM_KEY_SIZE + PM_IV_SIZE;
    unsigned char *key = malloc(keylen);

    PKCS5_PBKDF2_HMAC((const char *)mkey, PM_MKEY_SIZE, salt, PM_SALT_SIZE, PM_ROUNDS,
                      EVP_sha256(), keylen, key);

    EVP_CIPHER_CTX_init(ctx);
    EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, PM_IV_SIZE, NULL);
    EVP_DecryptInit_ex(ctx, NULL, NULL, key, key + PM_KEY_SIZE);

    free(key);
    
    return ctx;
}

int encryptToFileT(char *data, int datalen, unsigned char *mkey, const char *fname,
                   const char *path) {
    int cipherlen,
        tmplen;
    unsigned char *cipher = calloc(datalen + AES_BLOCK_SIZE, sizeof(char));
    unsigned char *tag = calloc(PM_TAG_SIZE, sizeof(char));
    
    unsigned char *salt = calloc(PM_SALT_SIZE, sizeof(char));
    RAND_bytes(salt, PM_SALT_SIZE);
    EVP_CIPHER_CTX *ctx = getEncryptCTX(mkey, salt);

    EVP_EncryptUpdate(ctx, cipher, &cipherlen, (unsigned char *)data, datalen);
    EVP_EncryptFinal_ex(ctx, cipher + cipherlen, &tmplen);

    cipherlen += tmplen;
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, PM_TAG_SIZE, tag);
        
    EVP_CIPHER_CTX_free(ctx);
    
    size_t flen = strlen(path) + strlen(fname) + 2;
    char *cfile = calloc(flen, sizeof(char)),
        *tfile = calloc(flen + 2, sizeof(char));
    snprintf(cfile, flen, "%s/%s", path, fname);
    snprintf(tfile, flen + 2, "%s/%s-t", path, fname);
    
    FILE *fp = fopen(cfile, "w");
    FILE *tfp = fopen(tfile, "w");
    free(cfile);
    free(tfile);

    int slen = 2*PM_SALT_SIZE + 2;
    char *str = calloc(slen, sizeof(char));
    char *p = str;
    
    for (int i = 0; i < PM_SALT_SIZE; i++, p += 2)
        snprintf(p, 3, "%02x", salt[i]);
    snprintf(p, 2, "\n");
    
    fprintf(fp, "%.*s%.*s", slen, str, cipherlen, cipher);
    fprintf(tfp, "%.*s", PM_TAG_SIZE, tag);

    free(str);
    free(tag);
    fclose(fp);
    fclose(tfp);
    free(salt);
    free(cipher);
    
    return 0;
}

int encryptToFile(userPass data, unsigned char *mkey, const char *fname, const char *path) {
    int txtlen = data.usersz + data.passsz + 2;
    char *txt = calloc(txtlen, sizeof(char));
    snprintf(txt, txtlen, "%s\n%s", data.user, data.pass);

    int res = encryptToFileT(txt, txtlen, mkey, fname, path);
    free(txt);

    return res;
}

char *decryptFromFile(unsigned char *mkey, const char *fname, const char *path) {
    char *salt = NULL,
        *str = NULL;
    size_t saltlen = 0;
    long offset = 0, bufsz = 0;
    int success = 0;
    unsigned char *extag = calloc(PM_TAG_SIZE + 1, sizeof(char));
    
    size_t flen = strlen(path) + strlen(fname) + 2;
    char *cfile = calloc(flen, sizeof(char)),
        *tfile = calloc(flen + 2, sizeof(char));
    snprintf(cfile, flen, "%s/%s", path, fname);
    snprintf(tfile, flen + 2, "%s/%s-t", path, fname);
    
    FILE *fp = fopen(cfile, "r");
    FILE *tfp = fopen(tfile, "r");
    free(cfile);
    free(tfile);

    fgets((char *)extag, PM_TAG_SIZE + 1, tfp);
    getline(&str, &saltlen, fp);
    salt = (char *)hexToBytes(str, 2*PM_SALT_SIZE);
    offset = ftell(fp);
    fseek(fp, 0, SEEK_END);
    bufsz = ftell(fp) - offset;
    fseek(fp, offset, SEEK_SET);

    char *buf = calloc(bufsz, sizeof(char));
    fread(buf, bufsz, 1, fp);
    fclose(fp);
    fclose(tfp);

    EVP_CIPHER_CTX *ctx = getDecryptCTX(mkey, (unsigned char *)salt);
    unsigned char *txt = calloc(bufsz, sizeof(char));
        
    int txtsz = bufsz, tmpsz = 0;
    
    EVP_DecryptUpdate(ctx, txt, &txtsz, (unsigned char *)buf, bufsz);
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, PM_TAG_SIZE, extag);
    success = EVP_DecryptFinal_ex(ctx, txt + txtsz, &tmpsz);
    txtsz += tmpsz;
    EVP_CIPHER_CTX_free(ctx);

    free(str);
    free(salt);
    free(extag);
    free(buf);

    if (success > 0)
        return (char *)txt;
    free(txt);

    return NULL;
}

char *genPass(size_t len) {
    srand((unsigned int)time(NULL));
    unsigned int inval = 15;

    char n[] = "0123456789";
    char low[] = "abcdefghijklmnoqprstuvwyzx";
    char up[] = "ABCDEFGHIJKLMNOQPRSTUYWVZX";
    char sym[] = "!@#$^&*?";

    char* pass = calloc(len + 1, sizeof(char));

    while (inval) {
        inval = 15;
        for (int i = 0; i < len; i++) {
            switch (rand() % 4) {
            case 0:
                pass[i] = low[rand() % 26];
                inval &= ~(0b1000);
                break;
            case 1:
                pass[i] = up[rand() % 26];
                inval &= ~(0b0100);
                break;
            case 2:
                pass[i] = n[rand() % 10];
                inval &= ~(0b0010);
                break;
            case 3:
                pass[i] = sym[rand() % 8];
                inval &= ~(0b0001);
                break;
            }
        }
    }
    
    pass[len] = '\0';

    return pass;
}

int storeSalt(const char *path, size_t pathlen) {
    size_t len = 2*PM_SALT_SIZE + 3;
    char *str = calloc(len, sizeof(char));
    char *p = str;
    unsigned char *salt = calloc(PM_SALT_SIZE, sizeof(char));

    RAND_bytes(salt, PM_SALT_SIZE);
    
    for (int i = 0; i < PM_SALT_SIZE; i++, p += 2)
        snprintf(p, 3, "%02x", salt[i]);
    snprintf(p, 3, "$\n");

    free(salt);
    const char *pwdfname = "salt";
    size_t pwdlen = pathlen + strlen(pwdfname) + 2;
    char *pwdfile = calloc(pwdlen, sizeof(char));
    snprintf(pwdfile, pwdlen, "%s/%s", path, pwdfname);
    FILE *fp = fopen(pwdfile, "w");
    free(pwdfile);
    fprintf(fp, "%s", str);
    free(str);
    
    return fclose(fp);
}

int checkFile(const char *path, const char *name) {
    int exists = 0;
    size_t len = strlen(path) + strlen(name) + 2;
    char *cfile = calloc(len, sizeof(char));
    snprintf(cfile, len, "%s/%s", path, name);

    if (access(cfile, R_OK) == 0)
        exists = 1;
    free(cfile);
    
    return exists;
}

unsigned char *authenticate(const char *pass, const char *path, size_t passlen,
                 size_t pathlen) {
    const char *pwdfname = "salt";
    size_t pwdlen = pathlen + strlen(pwdfname) + 2;
    size_t saltsz = 2*PM_SALT_SIZE + 2;
    char *pwdfile = calloc(pwdlen, sizeof(char));
    
    snprintf(pwdfile, pwdlen, "%s/%s", path, pwdfname);

    FILE *pwdfp = fopen(pwdfile, "r");
    free(pwdfile);
    char *salt = calloc(saltsz, sizeof(char));
    getdelim(&salt, &saltsz, '$', pwdfp);
    fclose(pwdfp);

    saltsz -= 2;
    salt[saltsz] = '\0';

    unsigned char *saltBytes = hexToBytes(salt, saltsz);
    saltsz /= 2;
    free(salt);
    
    unsigned char *key = passToKey(pass, saltBytes, passlen);
    free(saltBytes);
    
    return key;       
}

char *createConfigPath(const char *path) {
    struct stat st = {0};
    size_t plen = strlen(path) + 1;
    char *home = NULL;
    
    if (path[0] == '~' && path[1] == '/') {
        home = getenv("HOME");
        plen += strlen(home);
    }
     
    char *tmp = calloc(plen, sizeof(char));
    char *build = calloc(plen, sizeof(char));
    char *tok;
    int blen = 0;

    if (home)
        snprintf(tmp, plen, "/%s%s", home, path + 1);
    else
        strncpy(tmp, path, plen); 

    if (tmp[0] == '/')
        blen += snprintf(build, plen, "/");
    
    tok = strtok(tmp, "/");
    while (tok) {
        blen += snprintf(build + blen, plen - blen, "%s/", tok);
        if (stat(build, &st) == -1)
            mkdir(build, 0755);
        tok = strtok(NULL, "/");
    }
    free(tmp);
    return build;
}

char **parseDB(char *txt, size_t *dbsz) {
    if (!txt)
        return NULL;
    
    *dbsz = 256;
    size_t offset = 0;
    size_t bufsz = 0;
    int i = 0;
    char *buf;
    char **db = calloc(*dbsz, sizeof(char *));
    char c;

    while ((c = txt[offset + bufsz]) != '\0') {
        if (c == '\n') {
            if (bufsz > 0) {
                buf = calloc(++bufsz, sizeof(char));
                sscanf(txt + offset, "%[^\n]", buf);
                db[i++] = buf;
            }
            offset += bufsz;
            bufsz = 0;
        } else
            bufsz++;

        if (i == *dbsz) {
            *dbsz *= 2;
            db = reallocarray(db, *dbsz, sizeof(char *));
        }
    }

    db[i] = NULL;
    free(txt);

    return db;
}

int main(int argc, char *argv[]) {
    char *path = "~/.config/passMgr";
    if (argc > 1)
        path = argv[1];

    path = createConfigPath(path);
    char* pass;
    
    if (!checkFile(path, "db")) {
        printf("No data present, creating new user...\n");
        pass = getPass(1);
        if (!pass) {
            printf("Quitting\n");
            free(path);
            return -1;
        }
        storeSalt(path, strlen(path));
    } else
        pass = getPass(0);

    unsigned char *mkey = authenticate(pass, path, strlen(pass), strlen(path));
    free(pass);

    char **db;
    size_t dbsz = 0;
    if (!checkFile(path, "db")) {
        printf("Creating DB...\n");
        dbsz = 256;
        db = calloc(dbsz, sizeof(char *));
        db[0] = NULL;
    } else
        db = parseDB(decryptFromFile(mkey, "db", path), &dbsz);

    if (!db) {
        printf("Wrong password, quitting...\n");
        free(path);
        free(mkey);
        return -2;
    }

    printf("Pass Mgr v0.1\nType 'help' for help\n");

    size_t dblen = 0;
    while (db[dblen] != NULL)
        dblen++; 
    
    size_t inlen = 64,
        userlen = 128,
        pwdlen = 128,
        flen = 0;
    char *input = calloc(inlen, sizeof(char));
    char *userin = calloc(userlen, sizeof(char));
    char *pwdin;
    unsigned char *filehash;
    char *fname, *out;
    userPass data;
    int nReg = -1;
    input[0] = '\0';
    
    while (strlen(input) < 3 || strncmp(input, "exit", 4)) {
        printf("> ");
        if (getline(&input, &inlen, stdin) < 0)
            break;
        
        if (!strncmp("help", input, 4)) {
            printf("help:\tDisplay this message\n");
            printf("ls:\tShow all passwords\n");
            printf("store:\tStore a password by name\n");
            printf("update:\tUpdate an existing record by name\n");
            printf("gen:\tGenerate a password for a username\n");
            printf("get:\tRetrieve a password by name\n");
            printf("getn:\tRetreive a password by number\n");
            printf("exit:\tExit the program\n");
        }
        
        else if (!strncmp("ls", input, 2)) {
            for (int i = 0; i < dblen; i++)
                printf("%d\t%s\n", i + 1, db[i]); 
            printf("\n");
        }
        
        else if (!strncmp("store", input, 5)) {
            printf("Entry name: ");
            getline(&input, &inlen, stdin);
            filehash = passToHash(input, NULL, strlen(input) - 1, 0);
            fname = calloc(2*PM_HASH_SIZE + 1, sizeof(char));
            for (int i = 0; i < PM_HASH_SIZE; i++) 
                flen += snprintf(fname + flen, 2*PM_HASH_SIZE - flen, "%02x",
                                 filehash[i]);
            fname[flen] = '\0';

            if (!checkFile(path, fname)) {
                printf("User: ");
                getline(&userin, &userlen, stdin);
                pwdin = getPass(1);
                if (pwdin != NULL) {
                    data.usersz = strlen(userin) - 1;
                    data.passsz = strlen(pwdin);
                    userin[data.usersz] = '\0';
                    data.user = userin;
                    data.pass = pwdin;
                    inlen = strlen(input);
                    db[dblen] = calloc(inlen, sizeof(char));
                    strncpy(db[dblen], input, inlen - 1);
                    db[dblen][inlen - 1] = '\0';
                    dblen++;
                    
                    encryptToFile(data, mkey, fname, path);
   
                    memset(pwdin, 0, pwdlen);
                    free(pwdin);
                }
                memset(userin, 0, userlen);
            } else
                printf("Record already exists\n");
            
            flen = 0;
            free(fname);
            free(filehash); 
        }

        else if (!strncmp("update", input, 6)) {
            printf("Entry name: ");
            getline(&input, &inlen, stdin);
            filehash = passToHash(input, NULL, strlen(input) - 1, 0);
            fname = calloc(2*PM_HASH_SIZE + 1, sizeof(char));
            for (int i = 0; i < PM_HASH_SIZE; i++) 
                flen += snprintf(fname + flen, 2*PM_HASH_SIZE - flen, "%02x",
                                 filehash[i]);
            fname[flen] = '\0';

            if (checkFile(path, fname)) {
                printf("User: ");
                getline(&userin, &userlen, stdin);
                pwdin = getPass(1);
                if (pwdin != NULL) {
                    data.usersz = strlen(userin) - 1;
                    data.passsz = strlen(pwdin);
                    userin[data.usersz] = '\0';
                    data.user = userin;
                    data.pass = pwdin;
                    
                    encryptToFile(data, mkey, fname, path);
   
                    memset(pwdin, 0, pwdlen);
                    free(pwdin);
                }
                memset(userin, 0, userlen);
            } else
                printf("Record doesn't exist\n");
            
            flen = 0;
            free(fname);
            free(filehash); 
        }

        else if (!strncmp("getn", input, 4)) {
            printf("Entry number: ");
            getline(&input, &inlen, stdin);
            sscanf(input, "%d", &nReg);
            if (nReg > 0 && nReg <= dblen) {
                filehash = passToHash(db[nReg - 1], NULL, strlen(db[nReg - 1]), 0);
                fname = calloc(2*PM_HASH_SIZE + 1, sizeof(char));
                for (int i = 0; i < PM_HASH_SIZE; i++) 
                    flen += snprintf(fname + flen, 2*PM_HASH_SIZE - flen, "%02x",
                                     filehash[i]);
                fname[flen] = '\0';
                
                if (checkFile(path, fname)) {
                    out = decryptFromFile(mkey, fname, path);
                    if (out) {
                        printf("%s\n", out);
                        free(out);
                    } else
                        printf("Record is corrputed\n");
                } else
                    printf("Entry does not exist\n");

                free(fname);
                free(filehash);
                flen = 0;
            } else
                printf("Invalid input");
            nReg = -1;
            printf("\n");
        }
        
        else if (!strncmp("get", input, 3)) {
            printf("Entry name: ");
            getline(&input, &inlen, stdin);

            filehash = passToHash(input, NULL, strlen(input) - 1, 0);
            fname = calloc(2*PM_HASH_SIZE + 1, sizeof(char));
            for (int i = 0; i < PM_HASH_SIZE; i++) 
                flen += snprintf(fname + flen, 2*PM_HASH_SIZE - flen, "%02x",
                                 filehash[i]);
            fname[flen] = '\0';

            if (checkFile(path, fname)) {
                out = decryptFromFile(mkey, fname, path);
                if (out) {
                    printf("%s\n", out);
                    free(out);
                } else
                    printf("Record is corrputed\n");
            } else
                printf("Entry does not exist\n");

            free(fname);
            free(filehash);
            flen = 0;
            printf("\n");
        }
        
        else if (!strncmp("gen", input, 3)) {
            printf("Entry name: ");
            getline(&input, &inlen, stdin);

            filehash = passToHash(input, NULL, strlen(input) - 1, 0);
            fname = calloc(2*PM_HASH_SIZE + 1, sizeof(char));
            for (int i = 0; i < PM_HASH_SIZE; i++) 
                flen += snprintf(fname + flen, 2*PM_HASH_SIZE - flen, "%02x",
                                 filehash[i]);
            fname[flen] = '\0';

            if (!checkFile(path, fname)) {
                printf("User: ");
                getline(&userin, &userlen, stdin);
                printf("Generating password...\n");
                data.pass = genPass(32);
                data.passsz = strlen(data.pass);
                data.usersz = strlen(userin) - 1;;
                userin[data.usersz] = '\0';
                data.user = userin;
                inlen = strlen(input);
                db[dblen] = calloc(inlen, sizeof(char));
                strncpy(db[dblen], input, inlen - 1);
                db[dblen][inlen - 1] = '\0';
                dblen++;
  
                encryptToFile(data, mkey, fname, path);

                free(data.pass);
            } else
                printf("Record already exists\n");
            
            free(fname);
            free(filehash);
            flen = 0;
            memset(userin, 0, userlen);
        }
        else if (strncmp(input, "exit", 4))
            printf("Command not found. Type 'help' for help\n");

        if (dblen == dbsz) {
            dbsz *= 2;
            db = reallocarray(db, dbsz, sizeof(char *));
        }
    }

    printf("Quitting...\n");

    size_t nStoreDB = 1,
        offset = 0;
    for (int i = 0; i < dblen; i++)
        nStoreDB += strlen(db[i]) + 1;

    char *dbBuf = calloc(nStoreDB, sizeof(char));
    dbBuf[nStoreDB - 1] = '\0';
    for (int i = 0; i < dblen; i++) {
        offset += snprintf(dbBuf + offset, nStoreDB - offset, "%s\n", db[i]);
        free(db[i]);
    }

    free(db);
    encryptToFileT(dbBuf, nStoreDB, mkey, "db", path);

    free(dbBuf);
    free(input);
    free(userin);
    free(mkey);
    free(path);
    
    return 0;
}
