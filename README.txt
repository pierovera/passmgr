Video:

https://www.youtube.com/watch?v=eHtCTfaXjSY

Dependencies:

Any POSIX compiler should work, GCC is recommended.

openssl >=1.1.1

HOWTO:

Simply run 'make' to build the project.

Run 'make clean' to remove the binary.

Execute the program with './passmgr'.
